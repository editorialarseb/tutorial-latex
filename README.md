# Tutorial de LaTeX para gatos 🐈 #

Bienvenido al tutorial de LaTeX para gatos creado por Editorial Boris Ulianov. Aquí encontrarás los recursos necesarios para poder comenzar a escribir tus propios documentos utilizando este maravilloso sistema de composición tipográfica.

## Conceptos fundamentales ##

* [¿Qué es LaTeX?](https://es.wikibooks.org/wiki/Manual_de_LaTeX/Introducci%C3%B3n/Introducci%C3%B3n_a_LaTeX).
* [¿Cuál es la diferencia entre TeX y LaTeX?](https://bitbucket.org/editorialborisulianov/tutorial-latex/wiki/TeX%20y%20LaTeX).
* [¿Qué es un editor WYSIWYG?](https://es.wikipedia.org/wiki/WYSIWYG).

## Instalar LaTeX en tu computadora ##

* [Instalar en sistemas Linux](https://es.wikibooks.org/wiki/Manual_de_LaTeX/Introducci%C3%B3n/Instalaci%C3%B3n/Instalaci%C3%B3n_en_Linux).
* [Instalar en sistemas Windows](https://es.wikibooks.org/wiki/Manual_de_LaTeX/Introducci%C3%B3n/Instalaci%C3%B3n/Instalaci%C3%B3n_en_Windows).
* [Instalar en sistemas OSX](https://es.wikibooks.org/wiki/Manual_de_LaTeX/Introducci%C3%B3n/Instalaci%C3%B3n/Instalaci%C3%B3n_en_OS_X).

## Primeros pasos ##

** En desarrollo**
